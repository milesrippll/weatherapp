//
//  AppDelegate.swift
//  weatherapp
//
//  Created by Doug Chisholm on 11/02/2019.
//  Copyright © 2019 Rippll. All rights reserved.

// 🌂🕶🧤 

import UIKit
import UserNotifications
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate {

    var backgroundTaskID : UIBackgroundTaskIdentifier? = nil
    var window: UIWindow?
    let locationManager = CLLocationManager()
    var lastLocation : CLLocation? = nil
    var lastLocationUpdateStored : Date! = nil
    var firstLoad : Bool = false
    
    func getLocalValue(_ name : String) -> String {
        
        let key : String = name
        
        if(UserDefaults.standard.string(forKey: key) == nil)
        {
            return ""
        } else {
            let returnString : String = UserDefaults.standard.string(forKey: key)! as String
            return returnString.replacingOccurrences(of: "°", with: "")
        }
        
    }
    
    func getLocalDateValue(_ name : String) -> Date {
        
        let key : String = name
        
        if(UserDefaults.standard.string(forKey: key) == nil)
        {
            return Date().addingTimeInterval(60 * 60 * 24 * 7 * 30 * 12) // year from now as we dont know when they leave the house yet
        } else {
            let returnDate : Date = (UserDefaults.standard.object(forKey: "date") as? Date)!
            return returnDate
        }
        
    }
    
    // make sure this is localised so works in US
    func checkTimeForForecast() {
        let hour = Calendar.current.component(.hour, from: Date())
        var lastPush : String = "2018-12-12"
        
        let date = NSDate()
        var dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd" 
        var nowDateString = dateFormatter.string(from: date as Date)
        
        if (getLocalValue("lastDailyPushSet") != "") {
            lastPush = getLocalValue("lastDailyPushSet")
            
        }
        
        // if (nowDateString != lastPush) {
        if (hour > 4 && hour < 23) {
            print("getting forecast")  
            let weather : Weather = Weather()
            
            self.backgroundTaskID = UIApplication.shared.beginBackgroundTask (withName: "Finish Network Tasks") {
                // End the task if time expires.
                UIApplication.shared.endBackgroundTask(self.backgroundTaskID!)
                self.backgroundTaskID = UIBackgroundTaskIdentifier.invalid
            }
            
            weather.forecast() { (output) in
                
                print("BG TASK DONE")
                
                self.setForecastAlert()
                self.setClothingReminderForecastAlert()
                
                UserDefaults.standard.set(nowDateString, forKey: "lastDailyPushSet")  
                
                UIApplication.shared.endBackgroundTask(self.backgroundTaskID!)
                self.backgroundTaskID = UIBackgroundTaskIdentifier.invalid
            }
        }
        //}
        
       
        
             
    }
    
    // only called when app open?!
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("Recived: \(userInfo)")
        
        checkTimeForForecast()
        
        completionHandler(.newData)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print(deviceTokenString)
        
        /* prod Endpoint=sb://ns-weatherbot.servicebus.windows.net/;SharedAccessKeyName=DefaultListenSharedAccessSignature;SharedAccessKey=xTdaq2yvvF8zgaSD7fAi4hyDnwDlK4C/7HUmnOo0Lgo=#
         
         dev Endpoint=sb://ns-weatherbot.servicebus.windows.net/;SharedAccessKeyName=DefaultListenSharedAccessSignature;SharedAccessKey=lnYk50E3pN4dkJ/q2FmCaeSYXkwI0HLb5O9Ym++fgiY=
         
         */
        
        let hub = SBNotificationHub(connectionString:"Endpoint=sb://ns-weatherbot.servicebus.windows.net/;SharedAccessKeyName=DefaultListenSharedAccessSignature;SharedAccessKey=lnYk50E3pN4dkJ/q2FmCaeSYXkwI0HLb5O9Ym++fgiY=", notificationHubPath:"dev-weatherbot")
        hub!.registerNative(withDeviceToken: deviceToken, tags:nil, completion: { (error) in
            if (error != nil) {
                //print("Error registering for notification: \(error)")
            }
        })
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        print("i am not available in simulator \(error)")
    }
    
    func check() {
        UNUserNotificationCenter.current().getNotificationSettings(){ (settings) in
            
            switch settings.soundSetting{
            case .enabled:
                
                print("enabled sound setting")
                
            case .disabled:
                
                print("setting has been disabled")
                
            case .notSupported:
                print("something vital went wrong here")
            }
        }
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
  
         NotificationCenter.default.addObserver(self, selector: #selector(self.askForNotifications(notification:)), name: Notification.Name("askForNotifications"), object: nil)
        
       
        checkLocationAccess()
    
        setDefaults()
        
        return true
    }

    func checkLocationAccess() {
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            // Request when-in-use authorization initially
            //locationManager.requestWhenInUseAuthorization()
            setUpInitialLocationManager()
            break
            
        case .restricted, .denied:
            // Disable location features
            setUpInitialLocationManager()
            break
            
        case .authorizedAlways:
            // Enable location features
            self.setUpBackgroundLocationManager()
            UserDefaults.standard.set("1", forKey: "aiEnabled")  
            break
        case .authorizedWhenInUse:
            self.setUpInitialLocationManager()
            break
        }
    }
    
    func setDefaults() {
        if (getLocalValue("sunLatLon") == "") {
            UserDefaults.standard.set("0,0", forKey: "sunLatLon") 
            UserDefaults.standard.set("your location", forKey: "streetSunday") 
        }
        if (getLocalValue("monLatLon") == "") {
            UserDefaults.standard.set("0,0", forKey: "monLatLon") 
            UserDefaults.standard.set("your location", forKey: "streetMonday") 
        }
        if (getLocalValue("tueLatLon") == "") {
            UserDefaults.standard.set("0,0", forKey: "tueLatLon") 
            UserDefaults.standard.set("your location", forKey: "streetTuesday") 
        }
        if (getLocalValue("wedLatLon") == "") {
            UserDefaults.standard.set("0,0", forKey: "wedLatLon") 
            UserDefaults.standard.set("your location", forKey: "streetWednesday") 
        }
        if (getLocalValue("thuLatLon") == "") {
            UserDefaults.standard.set("0,0", forKey: "thuLatLon") 
            UserDefaults.standard.set("your location", forKey: "streetThursday") 
        }
        if (getLocalValue("friLatLon") == "") {
            UserDefaults.standard.set("0,0", forKey: "friLatLon") 
            UserDefaults.standard.set("your location", forKey: "streetFriday") 
        }
        if (getLocalValue("satLatLon") == "") {
            UserDefaults.standard.set("0,0", forKey: "satLatLon") 
            UserDefaults.standard.set("your location", forKey: "streetSaturday") 
        }
        
        
        if (getLocalValue("snowyIcon") == "") {
            UserDefaults.standard.set("❄️", forKey: "snowyIcon")  
        }
        if (getLocalValue("uvIcon") == "") {
            UserDefaults.standard.set("⛱", forKey: "uvIcon")  
        }
        if (getLocalValue("sunnyIcon") == "") {
            UserDefaults.standard.set("🕶", forKey: "sunnyIcon")  
        }
        if (getLocalValue("rainyIcon") == "") {
            UserDefaults.standard.set("🌂", forKey: "rainyIcon")  
        }
        if (getLocalValue("hotIcon") == "") {
            UserDefaults.standard.set("🔥", forKey: "hotIcon")  
        }
        if (getLocalValue("coldIcon") == "") {
            UserDefaults.standard.set("🧤", forKey: "coldIcon")  
        }
        if (getLocalValue("tempFormat") == "") {
            UserDefaults.standard.set("c", forKey: "tempFormat")  
        }
        
        if (getLocalValue("snowyValue") == "") {
            UserDefaults.standard.set("10", forKey: "snowyValue")  
        }
        if (getLocalValue("uvValue") == "") {
            UserDefaults.standard.set("4", forKey: "uvValue")  
        }
        if (getLocalValue("sunnyValue") == "") {
            UserDefaults.standard.set("sunny", forKey: "sunnyValue")  
        }
        if (getLocalValue("rainyValue") == "") {
            UserDefaults.standard.set("33", forKey: "rainyValue")  
        }
        if (getLocalValue("hotValue") == "") {
            // if c?
            UserDefaults.standard.set("73", forKey: "hotValue")  
        }
        if (getLocalValue("coldValue") == "") {
            // if c?
            UserDefaults.standard.set("37", forKey: "coldValue")
            
            if let tabBarController = self.window!.rootViewController as? UITabBarController {
                tabBarController.selectedIndex = 1
            }
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
       checkTimeForForecast()
    }
    
    func setClothingReminderForecastAlert() {
        let weather : Weather = Weather()
        var emojis : String = weather.getForecastHeaderEmojis()
        if (emojis == "") {
            return
        }
        let center = UNUserNotificationCenter.current()
        
        let content = UNMutableNotificationContent()
        
        content.title = "Hey there! " + weather.getForecastHeaderEmojis()
      
        var tips : String = ""
          let keywords : String = self.getLocalValue("keywords")
        
        if (keywords == "") {
            return // nothing to remind!
        }
        
        if keywords.contains("snow") {
            tips = tips + "boots "
        }
        if keywords.contains("rain") {
            tips = tips + "umbrella "
        }
        if keywords.contains("uv") {
            tips = tips + "sun cream "
        }
        if keywords.contains("hot") {
            tips = tips + "no jacket "
        }
        if keywords.contains("cold") {
            tips = tips + "woolies "
        }
        
        content.body = "Don't forget " + tips
       
        var leaveDate : Date = Date()
        
        if (Calendar.current.component(.weekday, from: Date()) == 1 ) {
            
            leaveDate = getLocalDateValue("timeLeavesHomeSunday")
                
        }
        if (Calendar.current.component(.weekday, from: Date()) == 2 ) {
            leaveDate = getLocalDateValue("timeLeavesHomeMonday")
        }
        if (Calendar.current.component(.weekday, from: Date()) == 3 ) {
            leaveDate = getLocalDateValue("timeLeavesHomeTuesday")
        }
        if (Calendar.current.component(.weekday, from: Date()) == 4 ) {
            leaveDate = getLocalDateValue("timeLeavesHomeWednesday")
        }
        if (Calendar.current.component(.weekday, from: Date()) == 5 ) {
            leaveDate = getLocalDateValue("timeLeavesHomeThursday")
        }
        if (Calendar.current.component(.weekday, from: Date()) == 6 ) {
            leaveDate = getLocalDateValue("timeLeavesHomeFriday")
        }
        if (Calendar.current.component(.weekday, from: Date()) == 7 ) {
            leaveDate = getLocalDateValue("timeLeavesHomeSaturday")
        }
        
        var timeBefore : Int = 10 // take from user setting
        
        let hour = Calendar.current.component(.hour, from: leaveDate.addingTimeInterval(TimeInterval(60 * timeBefore)))
        let minute = Calendar.current.component(.minute, from: leaveDate.addingTimeInterval(TimeInterval(60 * timeBefore)))
        
        
        var dateComponents = DateComponents()
        dateComponents.hour = hour
        dateComponents.minute = minute
 
        // dev mode
        /*
        let hour = Calendar.current.component(.hour, from: Date())
        let minute = Calendar.current.component(.minute, from: Date())
        let second = Calendar.current.component(.second, from: Date())
        dateComponents.hour = hour
        dateComponents.minute = minute
        dateComponents.second = second + 60
        */
        
        content.sound = UNNotificationSound.init(named: UNNotificationSoundName(rawValue: "reminder.wav"))
        
        let calTrigger : UNCalendarNotificationTrigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
        
        let identifier = "MyReminderLocalNotification" // This isnt unique yet so it will override the last one
        let request = UNNotificationRequest(identifier: identifier,
                                            content: content, trigger: calTrigger)
        center.add(request, withCompletionHandler: { (error) in
            if error != nil {
                // Something went wrong
            }
        })

    }
    
    // add shake to get forecast alert and reminder together
    func setForecastAlert() {
       
        let center = UNUserNotificationCenter.current()
         let weather : Weather = Weather()
        let content = UNMutableNotificationContent()
        content.title = "Good morning! " + weather.getForecastHeaderEmojis()
       
        content.body = weather.getForecastText()
        //"Today in Old Street it will be warmer than yesterday with a high of 12° and a low of 4°, you might need woollies and umbrella"
        content.sound = nil
        
        var dateComponents = DateComponents()
        dateComponents.hour = 6
        dateComponents.minute = 00
        
        // dev mode
        /*let hour = Calendar.current.component(.hour, from: Date())
        let minute = Calendar.current.component(.minute, from: Date())
        let second = Calendar.current.component(.second, from: Date())
        dateComponents.hour = hour
        dateComponents.minute = minute
        dateComponents.second = second + 3
         */
         
        
         let calTrigger : UNCalendarNotificationTrigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
        
        let identifier = "MyForecastLocalNotification" // This isnt unique yet so it will override the last one
        let request = UNNotificationRequest(identifier: identifier,
                                            content: content, trigger: calTrigger)
        center.add(request, withCompletionHandler: { (error) in
            if error != nil {
                // Something went wrong
            }
        })
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func setUpBackgroundLocationManager () {
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        
        locationManager.requestAlwaysAuthorization()
        locationManager.allowsBackgroundLocationUpdates = true
        
        locationManager.pausesLocationUpdatesAutomatically = false
        
        locationManager.startMonitoringSignificantLocationChanges()
        locationManager.distanceFilter = 100
    }
    
    @objc func askForNotifications(notification: Notification)  {
         
        print("ASK")
        
        if #available(iOS 10, *) {
            
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
                
                guard error == nil else {
                    //Display Error.. Handle Error.. etc..
                    return
                }
                
                if granted {
                    //Do stuff here..
                    
                    //Register for RemoteNotifications. Your Remote Notifications can display alerts now :)
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                }
                else {
                    //Handle user denying permissions..
                }
            }
            
            UIApplication.shared.registerForRemoteNotifications()
        }
        else {
            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            UIApplication.shared.registerForRemoteNotifications()
        }
    
        let center = UNUserNotificationCenter.current()
        let options: UNAuthorizationOptions = [.alert, .sound];
    
        center.requestAuthorization(options: options) {
            (granted, error) in
            if !granted {
                print("Something went wrong")
            } else {
                self.setUpBackgroundLocationManager()
            }
        }
        
        center.getNotificationSettings { (settings) in
            if settings.authorizationStatus != .authorized {
                // Notifications not allowed
            }
        }
    
    }
    
    func locationManager(_ manager: CLLocationManager, 
                         didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted, .denied:
            
            break
            
        case .authorizedWhenInUse:
            //
            UserDefaults.standard.set("1", forKey: "InitialLocationSet") 
            setUpInitialLocationManager()
            break
            
        case .authorizedAlways:
            //
            UserDefaults.standard.set("1", forKey: "BackgroundLocationSet") 
            setUpBackgroundLocationManager()
            break
            
        case .notDetermined:
            break
        }
    }
    

    func setUpInitialLocationManager () {
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.requestWhenInUseAuthorization()
        
        locationManager.startUpdatingLocation()
        self.firstLoad = true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) { 
         
        // only interested in leaving house and where you are at lunchtime
        let hour = Calendar.current.component(.hour, from: Date())
        if (hour < 5 && hour > 16) { 
            return
        }
        
        // stop micro movements
        if (lastLocation != nil) {
            let distance : Double = lastLocation!.distance(from: locations.last!)
            if (distance < 99.0) {
              // return
            }
        }
        
        var locationString : String = "0,0"
        let latString = String(format: "%f", locations.last!.coordinate.latitude)
        let lonString = String(format: "%f", locations.last!.coordinate.longitude)
        locationString = latString + "," + lonString
        
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: (locations.last?.coordinate.latitude)!, longitude: (locations.last?.coordinate.longitude)!)
        
        geoCoder.reverseGeocodeLocation(location) { (placemarks, error) in
            var placeMark : CLPlacemark!
            placeMark = placemarks![0]
            print(placeMark.name)
            var street : String = (placeMark.name?.replacingOccurrences(of: "1", with: ""))!
            street = street.replacingOccurrences(of: "2", with: "")
            street = street.replacingOccurrences(of: "3", with: "")
            street = street.replacingOccurrences(of: "4", with: "")
            street = street.replacingOccurrences(of: "5", with: "")
            street = street.replacingOccurrences(of: "6", with: "")
            street = street.replacingOccurrences(of: "7", with: "")
            street = street.replacingOccurrences(of: "8", with: "")
            street = street.replacingOccurrences(of: "9", with: "")
            street = street.replacingOccurrences(of: "0", with: "")
            
            let firstTwo : String = String(street.prefix(2))
            if (firstTwo == "A " || firstTwo == "B " || firstTwo == "C ") {
                street = String(street.dropFirst(2))
            }
            
            let hour = Calendar.current.component(.hour, from: Date())
            if (hour < 15 && hour > 11) {
                
                if (Calendar.current.component(.weekday, from: Date()) == 1 ) {
                    UserDefaults.standard.set(locationString, forKey: "sunLatLon") 
                    UserDefaults.standard.set(street, forKey: "streetSunday") 
                }
                if (Calendar.current.component(.weekday, from: Date()) == 2 ) {
                    UserDefaults.standard.set(locationString, forKey: "monLatLon") 
                    UserDefaults.standard.set(street, forKey: "streetMonday") 
                }
                if (Calendar.current.component(.weekday, from: Date()) == 3 ) {
                    UserDefaults.standard.set(locationString, forKey: "tueLatLon") 
                    UserDefaults.standard.set(street, forKey: "streetTuesday") 
                }
                if (Calendar.current.component(.weekday, from: Date()) == 4 ) {
                    UserDefaults.standard.set(locationString, forKey: "wedLatLon") 
                    UserDefaults.standard.set(street, forKey: "streetWednesday") 
                }
                if (Calendar.current.component(.weekday, from: Date()) == 5 ) {
                    UserDefaults.standard.set(locationString, forKey: "thuLatLon") 
                    UserDefaults.standard.set(street, forKey: "streetThursday") 
                }
                if (Calendar.current.component(.weekday, from: Date()) == 6 ) {
                    UserDefaults.standard.set(locationString, forKey: "friLatLon") 
                    UserDefaults.standard.set(street, forKey: "streetFriday") 
                }
                if (Calendar.current.component(.weekday, from: Date()) == 7 ) {
                    UserDefaults.standard.set(locationString, forKey: "satLatLon") 
                    UserDefaults.standard.set(street, forKey: "streetSaturday") 
                }
                
            }
            
            let nowTimeStamp = self.getCurrentTimeStamp()
            
            //self.setLocalNotification(title: "Location", body: "changed " + nowTimeStamp, devTime: 0, withSound : false)
            
            if (self.firstLoad) {
                self.firstLoad = false
                self.locationManager.stopUpdatingLocation()
                // set a cookie
                UserDefaults.standard.set("1", forKey: "InitialLocationSet") 
                
            }
            
            // check if leaving the house
            if (self.lastLocation != nil) {
                let distance : Double = self.lastLocation!.distance(from: locations.last!)
                if (distance > 99.0) {
                    
                    
                    let hour = Calendar.current.component(.hour, from: Date())
                    var lastLeavingHouse : String = "2018-12-12"
                    
                    let date = NSDate()
                    var dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd" 
                    var nowDateString = dateFormatter.string(from: date as Date)
                    
                    if (self.getLocalValue("lastLeaveHouseSet") != "") {
                        lastLeavingHouse = self.getLocalValue("lastLeaveHouseSet")
                        
                    }
                    
                    // looks like they moved 100m and the last time we saw them leave house wasnt today so lets do it
                    if (nowDateString != lastLeavingHouse) {
                         
                        // UserDefaults.standard.set(locations.last, forKey: "homeLocation") 
                            
                        if (Calendar.current.component(.weekday, from: Date()) == 1 ) {
                            UserDefaults.standard.set(Date(), forKey: "timeLeavesHomSunday") 
                            
                        }
                        if (Calendar.current.component(.weekday, from: Date()) == 2 ) {
                            UserDefaults.standard.set(Date(), forKey: "timeLeavesHomeMonday") 
                            
                        }
                        if (Calendar.current.component(.weekday, from: Date()) == 3 ) {
                            UserDefaults.standard.set(Date(), forKey: "timeLeavesHomeTuesday") 
                            
                        }
                        if (Calendar.current.component(.weekday, from: Date()) == 4 ) {
                            UserDefaults.standard.set(Date(), forKey: "timeLeavesHomeWednesday") 
                            
                        }
                        if (Calendar.current.component(.weekday, from: Date()) == 5 ) {
                            UserDefaults.standard.set(Date(), forKey: "timeLeavesHomeThursday") 
                            
                        }
                        if (Calendar.current.component(.weekday, from: Date()) == 6 ) {
                            UserDefaults.standard.set(Date(), forKey: "timeLeavesHomeFriday") 
                            
                        }
                        if (Calendar.current.component(.weekday, from: Date()) == 7 ) {
                            UserDefaults.standard.set(Date(), forKey: "timeLeavesHomeSaturday") 
                            
                        }
                             
                        UserDefaults.standard.set(nowDateString, forKey: "lastLeaveHouseSet") 
                        
                    }
                    
                    
                    
                }
                
            }
            
            
        }
        
        self.lastLocation = locations.last!
    
    }
    
    func getCurrentTimeStamp() -> String {
        let now = Date()
        
        let formatter = DateFormatter()
        
        formatter.timeZone = TimeZone.current
        
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateString = formatter.string(from: now)
        
        return dateString
    }
    
    func setLocalNotification(title: String, body : String, devTime : Int, withSound : Bool) {
        let center = UNUserNotificationCenter.current()
        
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = body
        
        if (withSound) {
           // content.sound = default
        } else {
            content.sound = nil
        }
        
        var trigger = UNTimeIntervalNotificationTrigger(timeInterval: 3,
                                                        repeats: false)
       
        let identifier = "MyLocalNotification" // This isnt unique yet so it will override the last one
        let request = UNNotificationRequest(identifier: identifier,
                                            content: content, trigger: trigger)
        center.add(request, withCompletionHandler: { (error) in
            if error != nil {
                // Something went wrong
            }
        })
        
        
        
    }

}

