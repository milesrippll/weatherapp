//
//  ViewController.swift
//  weatherapp
//
//  Created by Doug Chisholm on 11/02/2019.
//  Copyright © 2019 Rippll. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController {

    @IBOutlet weak var aiButton: UIButton!
    
    @IBOutlet weak var mainKeywordsLabel: UILabel!
    
    @IBOutlet weak var mainIconOne: UILabel!
    @IBOutlet weak var mainIconTwo: UILabel!
    @IBOutlet weak var mainIconThree: UILabel!
    
    @IBOutlet weak var myLabel: UILabel!
    @IBOutlet weak var mapOne: MKMapView!
    @IBOutlet weak var mapTwo: MKMapView!
    @IBOutlet weak var mapThree: MKMapView!
    @IBOutlet weak var mapFour: MKMapView!
    @IBOutlet weak var mapFive: MKMapView!
    
    @IBOutlet weak var day1TempMinLabel: UILabel!
    @IBOutlet weak var day1TempMaxLabel: UILabel!
    @IBOutlet weak var day2TempMinLabel: UILabel!
    @IBOutlet weak var day2TempMaxLabel: UILabel!
    @IBOutlet weak var day3TempMinLabel: UILabel!
    @IBOutlet weak var day3TempMaxLabel: UILabel!
    @IBOutlet weak var day4TempMinLabel: UILabel!
    @IBOutlet weak var day4TempMaxLabel: UILabel!
    @IBOutlet weak var day5TempMinLabel: UILabel!
    @IBOutlet weak var day5TempMaxLabel: UILabel!
    
    @IBOutlet weak var day1Label: UILabel!
    @IBOutlet weak var day2Label: UILabel!
    @IBOutlet weak var day3Label: UILabel!
    @IBOutlet weak var day4Label: UILabel!
    @IBOutlet weak var day5Label: UILabel!
    
    @IBOutlet weak var day1Icon1Label: UILabel!
    @IBOutlet weak var day1Icon2Label: UILabel!
    @IBOutlet weak var day1Icon3Label: UILabel!
    
    @IBOutlet weak var day2Icon1Label: UILabel!
    @IBOutlet weak var day2Icon2Label: UILabel!
    @IBOutlet weak var day2Icon3Label: UILabel!
    
    @IBOutlet weak var day3Icon1Label: UILabel!
    @IBOutlet weak var day3Icon2Label: UILabel!
    @IBOutlet weak var day3Icon3Label: UILabel!
    
    @IBOutlet weak var day4Icon1Label: UILabel!
    @IBOutlet weak var day4Icon2Label: UILabel!
    @IBOutlet weak var day4Icon3Label: UILabel!
    
    @IBOutlet weak var day5Icon1Label: UILabel!
    @IBOutlet weak var day5Icon2Label: UILabel!
    @IBOutlet weak var day5Icon3Label: UILabel!
    
    @IBOutlet weak var place1Label: UILabel!
    @IBOutlet weak var place2Label: UILabel!
    @IBOutlet weak var place3Label: UILabel!
    @IBOutlet weak var place4Label: UILabel!
    @IBOutlet weak var place5Label: UILabel!
    
    var todayIsCold : Bool = false
    var todayIsHot : Bool = false
    var todayIsRainy : Bool = false
    var todayIsSnowy : Bool = false
    var todayIsSunny : Bool = false
    var hundredMetersOn : Bool = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        loadContent()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        loadContent()
        
    }
    
    func loadContent() {
    
        let placeName : String = self.getLocalValue("street")
        let placeNameMonday : String = self.getLocalValue("streetMonday")
        let placeNameTuesday : String = self.getLocalValue("streetTuesday")
        let placeNameWednesday : String = self.getLocalValue("streetWednesday")
        let placeNameThursday : String = self.getLocalValue("streetThursday")
        let placeNameFriday : String = self.getLocalValue("streetFriday")
        let placeNameSaturday : String = self.getLocalValue("streetSaturday")
        let placeNameSunday : String = self.getLocalValue("streetSunday")
        
        if (Calendar.current.component(.weekday, from: Date()) == 1 ) {
            place1Label.text = placeNameMonday
            place2Label.text = placeNameTuesday
            place3Label.text = placeNameWednesday
            place4Label.text = placeNameThursday
            place5Label.text = placeNameFriday
        }
        if (Calendar.current.component(.weekday, from: Date()) == 2 ) {
            
            place1Label.text = placeNameTuesday
            place2Label.text = placeNameWednesday
            place3Label.text = placeNameThursday
            place4Label.text = placeNameFriday
            place5Label.text = placeNameSaturday
        }
        if (Calendar.current.component(.weekday, from: Date()) == 3 ) {
            
            
            place1Label.text = placeNameWednesday
            place2Label.text = placeNameThursday
            place3Label.text = placeNameFriday
            place4Label.text = placeNameSaturday
            place5Label.text = placeNameSunday
        }
        if (Calendar.current.component(.weekday, from: Date()) == 4 ) {
            
            place1Label.text = placeNameThursday
            place2Label.text = placeNameFriday
            place3Label.text = placeNameSaturday
            place4Label.text = placeNameSunday
            place5Label.text = placeNameMonday
        }
        if (Calendar.current.component(.weekday, from: Date()) == 5 ) {
        
            place1Label.text = placeNameFriday
            place2Label.text = placeNameSaturday
            place3Label.text = placeNameSunday
            place4Label.text = placeNameMonday
            place5Label.text = placeNameTuesday
        }
        if (Calendar.current.component(.weekday, from: Date()) == 6 ) {
            
            
            place1Label.text = placeNameSaturday
            place2Label.text = placeNameSunday
            place3Label.text = placeNameMonday
            place4Label.text = placeNameTuesday
            place5Label.text = placeNameWednesday
        }
        if (Calendar.current.component(.weekday, from: Date()) == 7) {
            
            
          
            place1Label.text = placeNameSunday
            place2Label.text = placeNameMonday
            place3Label.text = placeNameTuesday
            place4Label.text = placeNameWednesday
            place5Label.text = placeNameThursday
        }
        
        let weather : Weather = Weather()
        
       // weather.forecast(latitude: "51", longitude: "-0.1")

        
        weather.forecast() { (output) in
            
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
            
            self.day1TempMinLabel.text = self.getLocalValue("minTemperatureDay_1")
            self.day2TempMinLabel.text = self.getLocalValue("minTemperatureDay_2")
            self.day3TempMinLabel.text = self.getLocalValue("minTemperatureDay_3")
            self.day4TempMinLabel.text = self.getLocalValue("minTemperatureDay_4")
            self.day5TempMinLabel.text = self.getLocalValue("minTemperatureDay_5")
            self.day1TempMaxLabel.text = self.getLocalValue("maxTemperatureDay_1")
            self.day2TempMaxLabel.text = self.getLocalValue("maxTemperatureDay_2")
            self.day3TempMaxLabel.text = self.getLocalValue("maxTemperatureDay_3")
            self.day4TempMaxLabel.text = self.getLocalValue("maxTemperatureDay_4")
            self.day5TempMaxLabel.text = self.getLocalValue("maxTemperatureDay_5")
            
            self.resetIcons()
            let check : String = self.getLocalValue("minTemperatureDay_1")
            if (check == "") {
                
            } else {
                self.loadColdIcons()
                self.loadHotIcons() // should override cold (shouldnt be both!)
                self.loadRainyIcons()
                self.loadSnowIcons()// should override rainy 
                
                self.loadSunnyIcons()
                
            }
            
            let weather : Weather = Weather.init()
            
            self.myLabel.text = weather.getForecastText()
                
                
                //"Currently " + self.getLocalValue("currentTemperature") + " " + self.getLocalValue("summaryDay_0") + " Low of " + self.getLocalValue("minTemperatureDay_0") + " high of " + self.getLocalValue("maxTemperatureDay_0") 
            
            // tips
            
            var keywords = "today will be"
            if (self.todayIsSunny) {
                keywords = keywords + " sunny"
            }
            
            if (self.todayIsRainy) {
                keywords = keywords + " rainy"
            }
            
            if (self.todayIsSnowy) {
                keywords = keywords + " Snowy"
            }
            
            if (self.todayIsHot) {
                keywords = keywords + " hot"
            }
            
            if (self.todayIsCold) {
                keywords = keywords + " cold"
            }
            
            
            self.mainKeywordsLabel.text = weather.getForecastHeaderEmojis()
            
        //    UserDefaults.standard.set(keywords, forKey: "keywords")  
            
            
        })
        
        setDayLabels()
        
        let ai : String =  getLocalValue("aiEnabled")
        
        if(ai == "1") {
            
            aiButton.isHidden = true
        }
            
    }
    
    @IBAction func enableAI(sender: UIButton) {
        NotificationCenter.default.post(name: Notification.Name("askForNotifications"), object: nil)
    }
    
    @IBAction func setHotIcon(sender: UIButton) {
        // if 
        if(hundredMetersOn) {
            hundredMetersOn = false
            
        } else {
            hundredMetersOn = true
            
        }
    }
    
    // - hot / cold (icon 1)
    // - rain / snowy (icon 2)
    // sunny icon 3 (could be shades in any season!)
    
    func resetIcons() {
        self.todayIsSunny = false
         self.todayIsRainy = false
         self.todayIsSnowy = false
         self.todayIsHot = false
         self.todayIsCold = false
        
        self.day1Icon1Label.text = ""
         self.day1Icon2Label.text = ""
         self.day1Icon3Label.text = ""
         self.day2Icon1Label.text = ""
         self.day2Icon2Label.text = ""
         self.day2Icon3Label.text = ""
         self.day3Icon1Label.text = ""
         self.day3Icon2Label.text = ""
         self.day3Icon3Label.text = ""
         self.day4Icon1Label.text = ""
         self.day4Icon2Label.text = ""
         self.day4Icon3Label.text = ""
         self.day5Icon1Label.text = ""
         self.day5Icon2Label.text = ""
        self.day5Icon3Label.text = ""
        
         UserDefaults.standard.set("", forKey: "mainIconOne")
         UserDefaults.standard.set("", forKey: "mainIconTwo") 
         UserDefaults.standard.set("", forKey: "mainIconThree") 
        
          UserDefaults.standard.set("", forKey: "minTemperatureDay_1")
          UserDefaults.standard.set("", forKey: "minTemperatureDay_2")
          UserDefaults.standard.set("", forKey: "minTemperatureDay_3")
          UserDefaults.standard.set("", forKey: "minTemperatureDay_4")
          UserDefaults.standard.set("", forKey: "minTemperatureDay_5")
    }
    
    func loadColdIcons() {
        
        let cold : Double = Double(self.getLocalValue("coldValue"))!
        
        if (self.getLocalValue("minTemperatureDay_1") != "") {
            
            let minTemperatureDay_1 : Double = Double(self.getLocalValue("minTemperatureDay_1").replacingOccurrences(of: "°", with: ""))!
            
            if (minTemperatureDay_1 < cold) {
                self.day1Icon1Label.text = self.getLocalValue("coldIcon")
            }
        }
    
        let minTemperatureDay_2 : Double = Double(self.getLocalValue("minTemperatureDay_2").replacingOccurrences(of: "°", with: ""))!
        
        if (minTemperatureDay_2 < cold) {
            self.day2Icon1Label.text = self.getLocalValue("coldIcon")
        }
        
        let minTemperatureDay_3 : Double = Double(self.getLocalValue("minTemperatureDay_3").replacingOccurrences(of: "°", with: ""))!
      
        
        if (minTemperatureDay_3 < cold) {
            self.day3Icon1Label.text = self.getLocalValue("coldIcon")
        }
        
        let minTemperatureDay_4 : Double = Double(self.getLocalValue("minTemperatureDay_4").replacingOccurrences(of: "°", with: ""))!
        
        if (minTemperatureDay_4 < cold) {
            self.day4Icon1Label.text = self.getLocalValue("coldIcon")
        }
        
        let minTemperatureDay_5 : Double = Double(self.getLocalValue("minTemperatureDay_5").replacingOccurrences(of: "°", with: ""))!
        
        if (minTemperatureDay_5 < cold) {
            self.day5Icon1Label.text = self.getLocalValue("coldIcon")
        }
        
        let minTemperatureDay_0 : Double = Double(self.getLocalValue("minTemperatureDay_0").replacingOccurrences(of: "°", with: ""))!
        
        if (minTemperatureDay_0 < cold) {
          //  self.mainIconOne.text = self.getLocalValue("coldIcon")
            todayIsCold = true
            UserDefaults.standard.set(self.getLocalValue("coldIcon"), forKey: "mainIconOne")  
            
        }
    }
    
    func loadHotIcons() {
        
        let hot : Double = Double(self.getLocalValue("hotValue"))!
        
        if (self.getLocalValue("maxTemperatureDay_1") != "") {
            
            let maxTemperatureDay_1 : Double = Double(self.getLocalValue("maxTemperatureDay_1").replacingOccurrences(of: "°", with: ""))!
            
            if (maxTemperatureDay_1 > hot) {
                self.day1Icon1Label.text = self.getLocalValue("hotIcon")
            }
        }
        
        let maxTemperatureDay_2 : Double = Double(self.getLocalValue("maxTemperatureDay_2").replacingOccurrences(of: "°", with: ""))!
        
        if (maxTemperatureDay_2 > hot) {
            self.day2Icon1Label.text = self.getLocalValue("hotIcon")
        }
        
        let maxTemperatureDay_3 : Double = Double(self.getLocalValue("maxTemperatureDay_3").replacingOccurrences(of: "°", with: ""))!
        
        
        if (maxTemperatureDay_3 > hot) {
            self.day3Icon1Label.text = self.getLocalValue("hotIcon")
        }
        
        let maxTemperatureDay_4 : Double = Double(self.getLocalValue("maxTemperatureDay_4").replacingOccurrences(of: "°", with: ""))!
        
        if (maxTemperatureDay_4 > hot) {
            self.day4Icon1Label.text = self.getLocalValue("hotIcon")
        }
        
        let maxTemperatureDay_5 : Double = Double(self.getLocalValue("maxTemperatureDay_5").replacingOccurrences(of: "°", with: ""))!
        
        if (maxTemperatureDay_5 > hot) {
            self.day5Icon1Label.text = self.getLocalValue("hotIcon")
        }
        
        let maxTemperatureDay_0 : Double = Double(self.getLocalValue("maxTemperatureDay_0").replacingOccurrences(of: "°", with: ""))!
        
        if (maxTemperatureDay_0 > hot) {
          //  self.mainIconOne.text = self.getLocalValue("hotIcon")
            todayIsHot = true
            UserDefaults.standard.set(self.getLocalValue("hotIcon"), forKey: "mainIconOne") 
        }
    }
    
    func loadSnowIcons() {
        
        let snow : Double = 0.0 // Double(self.getLocalValue("snowyValue"))!
        
        let snowDay_1 : Double = Double(self.getLocalValue("snowDay_1"))!
        if (snow < snowDay_1) {
            self.day1Icon2Label.text = self.getLocalValue("snowyIcon")
        }
        
        let snowDay_2 : Double = Double(self.getLocalValue("snowDay_2"))!
        if (snow < snowDay_2) {
            self.day2Icon2Label.text = self.getLocalValue("snowyIcon")
        }
        
        let snowDay_3 : Double = Double(self.getLocalValue("snowDay_3"))!
        if (snow < snowDay_3) {
            self.day3Icon2Label.text = self.getLocalValue("snowyIcon")
        }
        
        let snowDay_4 : Double = Double(self.getLocalValue("snowDay_4"))!
        if (snow < snowDay_4) {
            self.day4Icon2Label.text = self.getLocalValue("snowyIcon")
        }
        
        let snowDay_5 : Double = Double(self.getLocalValue("snowDay_5"))!
        if (snow < snowDay_5) {
            self.day5Icon2Label.text = self.getLocalValue("snowyIcon")
        }
        
        let snowDay_0 : Double = Double(self.getLocalValue("snowDay_0"))!
        if (snow > 10) {
         //   self.mainIconTwo.text = self.getLocalValue("snowyIcon")
            todayIsSnowy = true
            UserDefaults.standard.set(self.getLocalValue("snowyIcon"), forKey: "mainIconTwo") 
        }
        
    }
    
    func loadRainyIcons() {
        
        let rain : Double = Double(self.getLocalValue("rainyValue"))!
        
        let rainDay_1 : Double = Double(self.getLocalValue("rainDay_1"))!
        if (rain < rainDay_1) {
            self.day1Icon2Label.text = self.getLocalValue("rainyIcon")
        }
        
        let rainDay_2 : Double = Double(self.getLocalValue("rainDay_2"))!
        if (rain < rainDay_2) {
            self.day2Icon2Label.text = self.getLocalValue("rainyIcon")
        }
        
        let rainDay_3 : Double = Double(self.getLocalValue("rainDay_3"))!
        if (rain < rainDay_3) {
            self.day3Icon2Label.text = self.getLocalValue("rainyIcon")
        }
        
        let rainDay_4 : Double = Double(self.getLocalValue("rainDay_4"))!
        if (rain < rainDay_4) {
            self.day4Icon2Label.text = self.getLocalValue("rainyIcon")
        }
        
        let rainDay_5 : Double = Double(self.getLocalValue("rainDay_5"))!
        if (rain < rainDay_5) {
            self.day5Icon2Label.text = self.getLocalValue("rainyIcon")
        }
        
        let rainDay_0 : Double = Double(self.getLocalValue("rainDay_0"))!
        if (rain < rainDay_0) {
            todayIsRainy = true
            UserDefaults.standard.set(self.getLocalValue("rainyIcon"), forKey: "mainIconTwo") 
        }
        
    }
    
    // if summary says sun then we turn it on...
    func loadSunnyIcons() {
        
        let sunny : Double = 0.0
        
        //Double(self.getLocalValue("sunnyValue"))!
        
        let sunDay_1 : Double = Double(self.getLocalValue("sunDay_1"))!
        if (sunny < sunDay_1) {
            self.day1Icon3Label.text = self.getLocalValue("sunnyIcon")
        }
        
        let sunDay_2 : Double = Double(self.getLocalValue("sunDay_2"))!
        if (sunny < sunDay_2) {
            self.day2Icon3Label.text = self.getLocalValue("sunnyIcon")
        }
        
        let sunDay_3 : Double = Double(self.getLocalValue("sunDay_3"))!
        if (sunny < sunDay_3) {
            self.day3Icon3Label.text = self.getLocalValue("sunnyIcon")
        }
        
        let sunDay_4 : Double = Double(self.getLocalValue("sunDay_4"))!
        if (sunny < sunDay_4) {
            self.day4Icon3Label.text = self.getLocalValue("sunnyIcon")
        }
        
        let sunDay_5 : Double = Double(self.getLocalValue("sunDay_5"))!
        if (sunny < sunDay_5) {
            self.day5Icon3Label.text = self.getLocalValue("sunnyIcon")
        }
        
        let sunDay_0 : Double = Double(self.getLocalValue("sunDay_0"))!
        if (sunny > 10) {
        //    self.mainIconThree.text = self.getLocalValue("sunnyIcon")
            todayIsSunny = true
            UserDefaults.standard.set(self.getLocalValue("sunnyIcon"), forKey: "mainIconThree") 
        }
    }
    
    
    func setDayLabels() {
        
        if (Calendar.current.component(.weekday, from: Date()) == 1 ) {
             day1Label.text = "M"
             day2Label.text = "T"
             day3Label.text = "W"
             day4Label.text = "T"
             day5Label.text = "F"
        }
        if (Calendar.current.component(.weekday, from: Date()) == 2 ) {
            day1Label.text = "T"
            day2Label.text = "W"
            day3Label.text = "T"
            day4Label.text = "F"
            day5Label.text = "S"
        }
        if (Calendar.current.component(.weekday, from: Date()) == 3 ) {
            day1Label.text = "W"
            day2Label.text = "T"
            day3Label.text = "F"
            day4Label.text = "S"
            day5Label.text = "S"
        }
        if (Calendar.current.component(.weekday, from: Date()) == 4 ) {
            day1Label.text = "T"
            day2Label.text = "F"
            day3Label.text = "S"
            day4Label.text = "S"
            day5Label.text = "M"
        }
        if (Calendar.current.component(.weekday, from: Date()) == 5 ) {
            day1Label.text = "F"
            day2Label.text = "S"
            day3Label.text = "S"
            day4Label.text = "M"
            day5Label.text = "T"
        }
        if (Calendar.current.component(.weekday, from: Date()) == 6 ) {
            day1Label.text = "S"
            day2Label.text = "S"
            day3Label.text = "M"
            day4Label.text = "T"
            day5Label.text = "W"
        }
        if (Calendar.current.component(.weekday, from: Date()) == 7 ) {
            day1Label.text = "S"
            day2Label.text = "M"
            day3Label.text = "T"
            day4Label.text = "W"
            day5Label.text = "T"
        }
    }
    
    func getLocalValue(_ name : String) -> String {
        
        let key : String = name
        
        if(UserDefaults.standard.string(forKey: key) == nil)
        {
            return ""
        } else {
            return UserDefaults.standard.string(forKey: key)!.replacingOccurrences(of: "°", with: "") as String
        }
        
    }


}

